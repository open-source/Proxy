#!/bin/bash

set -e

HELM_RELEASE_NAME=${HELM_RELEASE_NAME:0:53}
HELM_RELEASE_NAME=${HELM_RELEASE_NAME%%*(-)}

# Create/Update the secret
kubectl -n $KUBE_NAMESPACE create secret generic ${HELM_RELEASE_NAME} \
  --from-file=${ENV_PRODUCTION} \
  --dry-run=client \
  --save-config \
  -o yaml | \
  kubectl apply -f -
