#!/bin/sh

set -e

if [ ! -f .env ];
then
  cp .env.example .env
fi

sed -i 's/^APP_ENV=.*/APP_ENV=local/g' .env; 

# Make sure App Key is set
php artisan key:generate

# Generate helper files to fix inteliphense for laravel project
php artisan ide-helper:generate
php artisan ide-helper:meta

docker-php-entrypoint php-fpm