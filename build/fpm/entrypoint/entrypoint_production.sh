#!/bin/sh

set -e

# Production version will have the .env file mounted at /home/metager/.env
if [ -f /home/metager/.env ];
then
  cp /home/metager/.env .env
fi

php artisan optimize

docker-php-entrypoint php-fpm