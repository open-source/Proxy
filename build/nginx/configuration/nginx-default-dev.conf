resolver 127.0.0.11 valid=10s;

server {
    listen       8080;
    server_name  localhost;
    root   /mgproxy/mgproxy_app/public;
    index  index.php index.html index.htm;

    add_header Content-Security-Policy "default-src 'self'; script-src 'self' 'unsafe-inline'; script-src-elem 'self' 'unsafe-inline'; script-src-attr 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; style-src-elem 'self' 'unsafe-inline'; style-src-attr 'self' 'unsafe-inline'; img-src 'self' data:; font-src 'self'; connect-src 'self'; frame-src 'self'; frame-ancestors 'self'; form-action 'self'";
    client_max_body_size 30M;

    location /health-check/nginx { 
        access_log off;
        default_type text/plain;
        return 200 "healthy\n";
    }

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        set $fpm_server fpm;
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass $fpm_server:9000;
        fastcgi_index index.php;
        fastcgi_read_timeout 900;
        fastcgi_buffer_size    256k;
        fastcgi_buffers     32 256k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}