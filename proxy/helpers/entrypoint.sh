#!/bin/sh

set -e

if [ ! -f .env ];
then
  cp .env.example .env
  php artisan key:generate
fi

php artisan optimize

php-fpm7.4 -F -R