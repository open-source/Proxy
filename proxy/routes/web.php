<?php

use Illuminate\Http\Request;
use App\Http\Controllers\DownloadController;
use App\Http\Controllers\HealthcheckController;
use App\Http\Controllers\ProxyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(["prefix" => "health-check"], function () {
    Route::get("fpm", [HealthCheckController::class, "fpm"]);
    Route::get("worker", [HealthCheckController::class, "worker"]);
});


Route::group(['prefix' => 'download'], function () {
    Route::get('iframe-breakout', [DownloadController::class, 'iframeBreakout'])->name('download-iframe-breakout');
    Route::get('/', [DownloadController::class, 'download'])->name("download");
});

// Route for form submissions
Route::get('formget/{password}/{validUntil}/{url}', [ProxyController::class, 'formget'])
    ->name('proxy-formget')
    ->where('password', '[A-Fa-f0-9]{64}')
    ->where('url', '.*');
// Route without Proxy Header
Route::get('p/{host?}/{path?}', [ProxyController::class, 'proxy'])->name('proxy')->where('host', '[^\.]+(\.[^\.]+)+')->where('path', '(.*)');
// Route with Proxy Header
Route::get('{host?}/{path?}', [ProxyController::class, 'proxyPage'])->name('proxy-wrapper-page')->where('host', '[^\.]+(\.[^\.]+)+')->where('path', '(.*)');

Route::post('{host?}/{path?}', [ProxyController::class, 'urlgenerator'])->where('host', '[^\.]+(\.[^\.]+)+')->where('path', '(.*)');

/**
 * This is our old Proxy route
 * We will keep it temporarily to help users
 * migrate to the new ones and redirect to the new one.
 * 15.01.2021
 */
Route::get('{password}/{url}', function (Request $request, $password, $url) {
    $targetUrl = str_replace("<<SLASH>>", "/", $url);
    $targetUrl = str_rot13(base64_decode($targetUrl));
    if (strpos($targetUrl, URL::to('/')) === 0) {
        return redirect($targetUrl);
    }

    return redirect(\App\Http\Controllers\ProxyController::generateProxyWrapperUrl($targetUrl));
})->middleware('throttle:60:1')->middleware('checkpw');
# Route that triggers streaming of Download
Route::get('proxy/{password}/{id}/{url}', [ProxyController::class, 'proxy'])->middleware('checkpw:true');
Route::post('proxy/{password}/{id}/{url}', [ProxyController::class, 'streamFile'])->middleware('checkpw:true');


Route::post('/{url}', function ($url) {
    abort(405);
});

Route::get('metrics', function (Request $request) {
    // Only allow access to metrics from within our network
    $ip = $request->ip();
    $allowedNetworks = [
        "10.",
        "172.",
        "192.",
        "127.0.0.1",
    ];

    $allowed = false;
    foreach ($allowedNetworks as $part) {
        if (stripos($ip, $part) === 0) {
            $allowed = true;
        }
    }

    if (!$allowed) {
        abort(401);
    }

    $registry = \Prometheus\CollectorRegistry::getDefault();

    $renderer = new \Prometheus\RenderTextFormat();
    $result = $renderer->render($registry->getMetricFamilySamples());

    return response($result, 200)
        ->header('Content-Type', \Prometheus\RenderTextFormat::MIME_TYPE);
});
