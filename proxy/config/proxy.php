<?php

return [
    'password_old' => env("PROXY_PASSWORD_OLD", "old_secret"),
    'password' => env("PROXY_PASSWORD", "secret"),
    'cache' => [
        "enabled" => env("CACHE_ENABLED", true),
    ],
    'log' => [
        'location' => env("PROXY_LOG_LOCATION", "/dev/null"),
    ],
];