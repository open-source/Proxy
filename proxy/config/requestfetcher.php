<?php

return [
    'proxy' => [
        'host' => env("PROXY_HOST", ""),
        "port" => env("PROXY_PORT", ""),
        "user" => env("PROXY_USER", ""),
        "password" => env("PROXY_PASSWORD", "")
    ],
];