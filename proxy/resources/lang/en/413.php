<?php

return [
    'disclaimer.1' => 'The File size of the requested resource exceeds our configured file size limit. If you want you can chose to download this file. Your request will stay anonymous but we will not try to anonymize the contents.',
    'disclaimer.2' => 'For technical reasons you will need to confirm the download again on the next page.',
    'button' => 'Prepare Download',
];
