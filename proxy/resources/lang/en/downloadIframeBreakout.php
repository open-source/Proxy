<?php

return [
    'header' => 'Your file is ready to be downloaded',
    'downloadFile' => 'Download File'
];
