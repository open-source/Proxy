<?php

return [
    'disclaimer.1' => 'The File you requested is a resource for Download. You can proceed to download this file. Your request will stay anonymous but we will not try to anonymize the contents.',
    'disclaimer.2' => 'For technical reasons you will need to confirm the download again on the next page.',
    'button' => 'Prepare Download',
];
