<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Unknown Host</title>
    <style>
        body > div:nth-child(1) {
            max-width: 700px;
        }

        body {
            display: flex;
            justify-content: center;
            padding-top: 30vh;
        }

        body > div > a {
            border: 1px solid #fbcd79;
            max-width: 200px;
            padding: 4px 8px;
            text-align: center;
            color: inherit;
            text-decoration: none;
            border-radius: 5px;
            background-color: orange;
            color: white;
        }
    </style>
</head>
<body>
    <div>
        <p>{{$answer["message"]}}</p>
    </div>
</body>
</html>