@extends('layouts.app', ['key' => 'error'])

@section('content')
<div class="container content-container">
 	<h1>Operation too slow</h1>
	<p>
	{{ $exception->getMessage() }}
	</p>
</div>
@endsection
