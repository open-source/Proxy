<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The requested resource is too large</title>
    <style>
        body > div:nth-child(1) {
            max-width: 700px;
        }

        body {
            display: flex;
            justify-content: center;
            padding-top: 30vh;
        }

        body > div > a {
            border: 1px solid #fbcd79;
            max-width: 200px;
            padding: 4px 8px;
            text-align: center;
            color: inherit;
            text-decoration: none;
            border-radius: 5px;
            background-color: orange;
            color: white;
        }
    </style>
</head>
<body>
    <div>
        <p>@lang('413.disclaimer.1')</p>
        <p>@lang('413.disclaimer.2')</p>
        <a href="{{ route('download-iframe-breakout', ['url' => $url, 'valid-until' => $validuntil, 'password' => $password]) }}" target="_top">
            @lang('413.button')
        </a>
    </div>
</body>
</html>