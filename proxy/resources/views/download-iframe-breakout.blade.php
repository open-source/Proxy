@extends('layouts.app', ['targetUrl' => $url])

@section('content')

<style>
    main {
    display: flex;
    align-items: center;
    justify-content: center;
    padding-top: 30vh;
    }

    body > main > div {
    max-width: 700px;
    }

    body > main > div > a {
        border: 1px solid #fbcd79;
        max-width: 200px;
        padding: 4px 8px;
        text-align: center;
        color: inherit;
        text-decoration: none;
        border-radius: 5px;
        background-color: orange;
        color: white;
    }

    body > main > div > a:hover {
        text-decoration: none;
        color: white;
    }
</style>

<main>
    <div>
        <h3>@lang('downloadIframeBreakout.header')</h3>
        <a href="{{ route('download', ['url' => $url, 'valid-until' => $validUntil, 'password' => $password]) }}">@lang('downloadIframeBreakout.downloadFile')</a>
    </div>
</main>

@endsection
