<?php

namespace App;

class PrometheusExporter {

    const NAMESPACE = "proxy";

    /**
     * Registers page calls not counting subdocument (css, js, etc)
     */
    static function registerProxyCall() {
        $registry = \Prometheus\CollectorRegistry::getDefault();
        $counter = $registry->getOrRegisterCounter(self::NAMESPACE , 'pages', 'counts calls to the proxy', []);
        $counter->inc();
    }

    /**
     * Registers all proxied documents and their type
     */
    static function registerProxyPageCall($type) {
        $registry = \Prometheus\CollectorRegistry::getDefault();
        $counter = $registry->getOrRegisterCounter(self::NAMESPACE , 'documents', 'counts proxied documents (html, js, css,...)', ['type']);
        $counter->incBy(1, [$type]);
    }

    static function registerDownload() {
        $registry = \Prometheus\CollectorRegistry::getDefault();
        $counter = $registry->getOrRegisterCounter(self::NAMESPACE , 'download', 'counts streamed downloads', []);
        $counter->inc();
    }
}