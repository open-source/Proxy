<?php

namespace App;

use URL;

abstract class Document
{
    protected $password;
    protected $baseUrl;

    public function __construct($password, $base)
    {
        $this->password = $password;
        $this->baseUrl  = $base;
    }

    public function proxifyFormAction($url){
        $url = trim($url);
        if (stripos($url, "javascript:") === 0) {
            return "";
        }

        // Deny Loading internal URLs and check if URL syntax is correct
        $host = parse_url($url, PHP_URL_HOST);
        $selfHost = \Request::getHttpHost();
        if (strpos($url, "http") !== 0 || $host === false || $host === $selfHost) {
            return $url;
        }

        return \App\Http\Controllers\ProxyController::generateFormgetUrl($url);
    }

    public function proxifyUrl($url, $topLevel)
    {
        // Only convert valid URLs
        $url = trim($url);

        if (stripos($url, "javascript:") === 0) {
            return "";
        }

        // Deny Loading internal URLs and check if URL syntax is correct
        $host = parse_url($url, PHP_URL_HOST);
        $selfHost = \Request::getHttpHost();
        if (strpos($url, "http") !== 0 || $host === false || $host === $selfHost) {
            return $url;
        }

        if($topLevel){
            return \App\Http\Controllers\ProxyController::generateProxyWrapperUrl($url);
        }else{
            return \App\Http\Controllers\ProxyController::generateProxyUrl($url);
        }
    }

    protected function convertRelativeToAbsoluteLink($rel)
    {
        if (strpos($rel, "//") === 0) {
            $rel = parse_url($this->baseUrl, PHP_URL_SCHEME) . ":" . $rel;
        }

        /* return if already absolute URL or empty URL */
        if (parse_url($rel, PHP_URL_SCHEME) != ''
            || strlen(trim($rel)) <= 0
            || preg_match("/^\s*mailto:/si", $rel)) {
            return ($rel);
        }

        /* queries and anchors */
        if ($rel[0] == '#' || $rel[0] == '?') {
            return ($this->baseUrl . $rel);
        }

        /* parse base URL and convert to local variables:
        $scheme, $host, $path */
        extract(parse_url($this->baseUrl));

        /* remove non-directory element from path */
        if (isset($path)) {
            $path = preg_replace('#/[^/]*$#', '', $path);
        }

        /* destroy path if relative url points to root */
        if ($rel[0] == '/') {
            $path = '';
        }

        /* dirty absolute URL */
        $abs = '';

        /* do we have a user in our URL? */
        if (isset($user)) {
            $abs .= $user;

            /* password too? */
            if (isset($pass)) {
                $abs .= ':' . $pass;
            }

            $abs .= '@';
        }

        if (!isset($host)) {
            #die(var_dump($this->baseUrl));
        }

        $abs .= $host;
        /* did somebody sneak in a port? */
        if (isset($port)) {
            $abs .= ':' . $port;
        }

        if (isset($path)) {
            $abs .= $path;
        }
        if (isset($rel)) {
            $abs .= "/" . ltrim($rel, "/");
        }
        /* replace '//' or '/./' or '/foo/../' with '/' */
        $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
        for ($n = 1; $n > 0; $abs = preg_replace($re, '/', $abs, -1, $n)) {
        }

        /* absolute URL is ready! */
        return ($scheme . '://' . $abs);
    }

    abstract public function proxifyContent();
}
