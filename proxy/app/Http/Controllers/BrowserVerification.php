<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class BrowserVerification extends Controller
{
    public function verifyCss(Request $request)
    {
        $key = $request->input('id', '');
        if (!preg_match("/^[a-f0-9]{32}$/", $key)) {
            abort(404);
        }

        $redis = Redis::connection("central");
        $redis->pipeline(function ($pipe) use ($key) {
            $pipe->lpush($key, 1);
            $pipe->expire($key, 60);
        });

        return response("", 200)
            ->header("Content-Type", "text/css");
    }
}
