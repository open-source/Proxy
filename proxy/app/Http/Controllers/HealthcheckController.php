<?php

namespace App\Http\Controllers;

use App\Console\Commands\RequestFetcher;
use Illuminate\Support\Facades\Redis;

class HealthcheckController extends Controller
{
    public function fpm()
    {
        return response('OK', 200)
            ->header('Content-Type', 'text/plain');
    }

    public function worker()
    {
        $last_heartbeat = Redis::get(RequestFetcher::HEALTHCHECK_KEY);

        if ($last_heartbeat === false) {
            abort(500, "No Heartbeat yet");
        }

        if (now()->timestamp - $last_heartbeat < 5) {
            return response("OK", 200, ["Content-Type" => "text/plain"]);
        } else {
            abort(500, "Heartbeat too long ago");
        }
    }
}
