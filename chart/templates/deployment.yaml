apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "chart.fullname" . }}
  labels:
    {{- include "chart.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "chart.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "chart.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "chart.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      volumes:
      - name: env-files
        secret:
          secretName: {{ template "secret_name" . }}
      - name: redis-config
        configMap:
          name: {{ include "chart.fullname" . }}-redis-container-config
      containers:
        - name: {{ .Chart.Name }}-phpfpm
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ template "fpm_image" . }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          env:
          - name: APP_ENV
            value: {{ .Values.environment }}
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health-check/fpm
              port: http
          readinessProbe:
            httpGet:
              path: /health-check/fpm
              port: http
          startupProbe:
            tcpSocket:
              port: 9000
            failureThreshold: 60
            periodSeconds: 1
          resources:
            requests:
              cpu: 500m
              memory: 500M
          volumeMounts:
          - name: env-files
            mountPath: /mgproxy/mgproxy_app/.env
            subPath: ENV_PRODUCTION
            readOnly: true
        - name: worker
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ template "fpm_image" . }}"
          command: ["/usr/local/bin/php"]
          args: ["artisan", "requests:fetcher"]
          imagePullPolicy: {{ .Values.image.fpm.pullPolicy }}
          env:
          - name: APP_ENV
            value: {{ .Values.environment }}
          volumeMounts:
          - name: env-files
            mountPath: /mgproxy/mgproxy_app/.env
            subPath: ENV_PRODUCTION
            readOnly: true
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health-check/worker
              port: http
          readinessProbe:
            httpGet:
              path: /health-check/worker
              port: http
          startupProbe:
            httpGet:
              path: /health-check/worker
              port: http
          resources:
            requests:
              cpu: 500m
              memory: 256M
            limits:
        - name: nginx
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ template "nginx_image" . }}"
          imagePullPolicy: {{ .Values.image.nginx.pullPolicy }}
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /health-check/nginx
              port: http
          readinessProbe:
            httpGet:
              path: /health-check/nginx
              port: http
          startupProbe:
            httpGet:
              path: /health-check/nginx
              port: http
            failureThreshold: 60
            periodSeconds: 1
          resources:
            requests:
              cpu: 100m
              memory: 100M
            limits:
        - name: redis
          image: "redis:6"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command: ["redis-server", "/usr/local/etc/redis/redis.conf"]
          volumeMounts:
          - name: redis-config
            mountPath: /usr/local/etc/redis/redis.conf
            subPath: redis.conf
            readOnly: true
          livenessProbe:
            exec:
              command:
              - redis-cli
              - ping 
            initialDelaySeconds: 2
          readinessProbe:
            exec:
              command:
              - redis-cli
              - ping 
            initialDelaySeconds: 2
          resources:
            requests:
              cpu: 100m
              memory: 1Gi
            limits:
          securityContext:
            runAsUser: 999
            runAsGroup: 999
            allowPrivilegeEscalation: false      
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 50
              podAffinityTerm:
                labelSelector:
                  matchLabels:
                    app.kubernetes.io/instance: {{ .Release.Name }}
                topologyKey: kubernetes.io/hostname
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
